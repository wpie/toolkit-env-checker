#include <bitset>
#include <chrono>
#include <cstdlib>
#include <exception>
#include <filesystem>
#include <functional>

#include <iostream>

bool IsIgnoreGui() {
  return std::filesystem::exists(std::string{getenv("HOME")} +
                                 "/IGNORE_GUI_CHECK");
}

#include "print"
void PrintResult(std::string_view item, const std::function<bool()> &check) {
  std::print("{} Check ", item);
  std::print("[{}]\n", check() ? "PASS" : "FAIL");
  std::print("===================================================\n");
}

void Check(std::string_view item, const std::function<void()> &check) {
  std::print("{} Checking...\n", item);
  check();
  std::print("\n{} Check [COMPLETE]\n", item);
  std::print("===================================================\n");
}

bool CheckDocker() { return std::filesystem::exists("/.dockerenv"); }

bool CheckDaHengSdk() {
  return std::filesystem::exists("/usr/lib/libgxiapi.so");
}

bool CheckRos() {
  return std::system("/opt/ros/jazzy/bin/ros2 > /dev/null") == 0 &&
         std::filesystem::exists("/opt/ros/jazzy/lib/librosbag2_cpp.so");
}

#include "bit"

void CheckCpp26() {
  if (std::bitset<16>(2333).to_string() != "0000100100011101" ||
      __cplusplus < 202300L)
    std::terminate();
}

#include "openvino/openvino.hpp"

void print_any_value(const ov::Any &value) {
  if (value.empty()) {
    std::print("EMPTY VALUE\n");
  } else {
    const auto &stringValue = value.as<std::string>();
    std::print("{}\n", stringValue.empty() ? "\"\"" : stringValue);
  }
}

void CheckOpenVino() {
  std::cout << ov::get_openvino_version() << std::endl;

  // -------- Step 1. Initialize OpenVINO Runtime Core --------
  ov::Core core;

  // -------- Step 2. Get list of available devices --------
  std::vector<std::string> availableDevices = core.get_available_devices();

  // -------- Step 3. Query and print supported metrics and config keys --------
  std::cout << "Available devices: " << std::endl;
  for (auto &&device : availableDevices) {
    std::cout << device << std::endl;
    if (device.find("CPU") != std::string::npos ||
        device.find("GPU") != std::string::npos) {
      // ONLY Query supported properties of CPU or GPU
      std::cout << "\tSUPPORTED_PROPERTIES: " << std::endl;
      auto supported_properties =
          core.get_property(device, ov::supported_properties);
      for (auto &&property : supported_properties) {
        if (property != ov::supported_properties.name()) {
          std::cout << "\t\t"
                    << (property.is_mutable() ? "Mutable: " : "Immutable: ")
                    << property << " : " << std::flush;
          print_any_value(core.get_property(device, property));
        }
      }
      std::cout << std::endl;
    }
  }
}

#include <Eigen/Eigen>

void CheckEigen() {
  Eigen::MatrixXd M1 = Eigen::MatrixXd::Random(3, 3);
  M1 = (M1 + Eigen::MatrixXd::Constant(3, 3, 1.0)) * 50;
  std::cout << "M1 =" << std::endl << M1 << std::endl;

  Eigen::MatrixXd M2 = Eigen::MatrixXd::Random(3, 3);
  M2 = Eigen::MatrixXd::Constant(3, 3, 2.0);
  std::cout << "M2 =" << std::endl << M2 << std::endl;

  std::cout << "M1 *M2 =" << std::endl << M1 * M2 << std::endl;
}

#include <opencv2/opencv.hpp>

void CheckOpenCv() {
  if (IsIgnoreGui()) {
    cv::Mat a(16, 16, CV_8UC3);
    cv::RNG rng;
    rng.fill(a, cv::RNG::UNIFORM, 0, 256);
    std::cout << a << std::endl;
  } else {
    cv::Mat a(320, 320, CV_8UC3);
    cv::RNG rng;
    rng.fill(a, cv::RNG::UNIFORM, 0, 256);
    imshow("a", a);
    cv::waitKey(0);
  }
}

#include "fmt/chrono.h"
#include "fmt/format.h"
#include "fmt/ranges.h"

void CheckFmt() { fmt::print("Now is {}", std::chrono::system_clock::now()); }

#include "usb.h"

void CheckUsb() {
  usb_init();
  fmt::print("busses count: {}\n", usb_find_busses());
  fmt::print("devices count: {}\n", usb_find_devices());
}

#include "hidapi/hidapi.h"

void CheckHidApi() {
  hid_init();
  fmt::print("hid version: {}.{}.{}", hid_version()->major,
             hid_version()->minor, hid_version()->patch);
}

#include "libserial/SerialPort.h"

void CheckSerial() {
  LibSerial::SerialPort port{};
  fmt::print("{}", port.GetAvailableSerialPorts());
}

#include "boost/logic/tribool.hpp"

void CheckBoost() {
  boost::tribool tb(true);
  if (tb) {
    fmt::print("true\n");
  }
  tb = boost::indeterminate;        // tb2是不确定状态
  if (tb == boost::indeterminate) { // 与indeterminate比较，无意义
    fmt::print("indeterminate\n");  // 不会输出
  }

  if (indeterminate(tb)) {           // 用indeterminate()函数检测状态
    fmt::print("indeterminate()\n"); // 输出indeterminate()
  }
}

#include "nlohmann/json.hpp"

void CheckNlohmannJson() {
  std::string info = R"({"A" : "a", "B" : "b", "Pi" : 1234 })";
  auto config_json = nlohmann::json::parse(info);
  std::cout << config_json["Pi"] << std::endl;
  config_json["Env"] = "vision-dev";
  std::cout << config_json.dump();
}

#include <ceres/ceres.h>
#include <ceres/rotation.h>

struct CostFunctor {
  template <typename T> bool operator()(const T *const x, T *residual) const {
    residual[0] = 10.0 - x[0];
    return true;
  }
};

void CheckCeres() {
  // The variable to solve for with its initial value.
  double initial_x = 5.0;
  double x = initial_x;

  // Build the problem.
  ceres::Problem problem;

  // Set up the only cost function (also known as residual). This uses
  // auto-differentiation to obtain the derivative (jacobian).
  ceres::CostFunction *cost_function =
      new ceres::AutoDiffCostFunction<CostFunctor, 1, 1>(new CostFunctor);
  problem.AddResidualBlock(cost_function, nullptr, &x);

  // Run the solver!
  ceres::Solver::Options options;
  options.linear_solver_type = ceres::DENSE_QR;
  options.minimizer_progress_to_stdout = true;
  ceres::Solver::Summary summary;
  Solve(options, &problem, &summary);

  std::cout << summary.FullReport() << "\n";
  std::cout << "x : " << initial_x << " -> " << x << "\n";
}

void CheckX11Forwarding() {
  if (IsIgnoreGui())
    return;
  system("apt install x11-apps -y > /dev/null");
  system("xclock");
}

int main() {
  std::print("Welcome to VisionDev Container Environment Checker\n");
  std::print("===================================================\n\n");
  PrintResult("Docker Container Environment", CheckDocker);
  PrintResult("DaHengSdk Installation", CheckDaHengSdk);
  PrintResult("Ros Installation", CheckRos);
  Check("X11 Forwarding", CheckX11Forwarding);
  Check("C++26 Support", CheckCpp26);
  Check("OpenVino Use", CheckOpenVino);
  Check("Eigen Use", CheckEigen);
  Check("OpenCv Use", CheckOpenCv);
  Check("Fmt Use", CheckFmt);
  Check("Usb Use", CheckUsb);
  Check("HidApi Use", CheckHidApi);
  Check("Serial Use", CheckSerial);
  Check("Boost Use", CheckBoost);
  Check("Nlohmann Json Use", CheckNlohmannJson);
  Check("Ceres Use", CheckCeres);
  std::print("\nVisionDev Container Environment Check Complete.\n");
  std::print("Please Enjoy Your Development Journey.\n");
  std::print("Good Luck.\n");
  return 0;
}